<?php

header('Content-Type: text/html; charset=UTF-8');


session_start();


if (!empty($_SESSION['login'])) {

  session_destroy();
  if($_COOKIE['admin']=='1') {
    setcookie('admin','0');
    header('Location:admin.php');
}
else {
    header('Location:index.php');
}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if(!empty($_SESSION['error_session'])){
      $msg = $_SESSION['error_session'];
      print("<div>$msg</div>");
      $_SESSION['error_session']="";
  }
  ?>
  <html>
  <head>
  <link rel="stylesheet" href="style.css" media="all">
  <title>Login</title>
  </head>
  <body>
  <form action="login.php" method="post" class = "block">
      <p>login</p><input name="login" />
      <p>password</p><input name="pass" type="password"/>
      <input type="submit" value="Войти" />
  </form>
  </body>

  </html>
  <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  include 'dblogin.php';
 
// подготовленный запрос для поля с логином
    $login=strip_tags($_POST['login']);
    $stmt = $db->prepare("SELECT id,login,pass FROM form6 WHERE login=?");
    $stmt->execute(array($login));
  
    $row = $stmt->fetch();
    $f=0;
        if (password_verify($_POST['pass'],$row['pass']))
        {
            $f=1;
            $_SESSION['login'] = strip_tags($_POST['login']);
            $_SESSION['uid'] = $id;
            header('Location: index.php');
        }
    else{
        $_SESSION['error_session'] = "Bad pass";
        header('Location: login.php');
        exit();
    }
    if($f==0){
        $_SESSION['error_session'] = "Bad login";
        header('Location:login.php');
        exit();
    }
    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = strip_tags($_POST['login']);
    // Записываем ID пользователя.
    $_SESSION['uid'] = $id;
    // Делаем перенаправление.
    header('Location: index.php');
}
