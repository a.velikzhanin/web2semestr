<?php
// проверка на авторизованность админа
function isAdmin($db){
    //авторизация админа
    if (empty($_SERVER['PHP_AUTH_USER']) ||
        empty($_SERVER['PHP_AUTH_PW'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="666"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    //форма админа, проверка
    $request = "SELECT * from admin";
    $adminResult = $db->prepare($request);
    $adminResult->execute();
    $l=0;
    while($data=$adminResult->fetch()){
        if($data['login']==strip_tags($_SERVER['PHP_AUTH_USER']) && strip_tags($_SERVER['PHP_AUTH_PW'])==$data['pass']){
            $l=1;
        }
    }
    // если админ забыл пароль или негодник залез попытался зайти
    if($l==0){
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab6"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
}

// безопасный вывод данных через strip_tags()
function print_data($data){
    print '<tr><td>';
    print $data['id'];
    print '</td><td>';
    print strip_tags($data['fio']);
    print '</td><td>';
    print strip_tags($data['email']);
    print '</td><td>';
    print strip_tags($data['bdate']);
    print '</td><td>';
    print strip_tags($data['sex']);
    print '</td><td>';
    print intval($data['limb']);
    print '</td><td>';
    print strip_tags($data['bio']);
    print '</td><td>';
    print strip_tags($data['login']);
    print '</td><td>';
    print strip_tags($data['pass']);
    print '</td>';
}
// вывод сверхспособностей
function print_superpower($abil){
    if($abil=='godmode')print 'Godmode';
    elseif($abil=='wallhack')print 'Wallhack';
    elseif($abil =='noclip')print 'Noclip';
    else print 'Error';
    print '<br/>';
}
?>
