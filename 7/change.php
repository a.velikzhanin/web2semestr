<?php
header('Content-Type: text/html; charset=UTF-8');
include 'dblogin.php';
include 'scripts.php';
isAdmin($db);
//$id =$_GET['id'];
//$id = $db->quote($_GET['id']);
$id = strip_tags($_POST['id']);
$stmt = $db->prepare("SELECT login FROM form6 WHERE id = ?");
$stmt->execute(array($id));
$user_login='';
while($row = $stmt->fetch())
{
    $user_login=$row['login'];
}

$request = "SELECT * FROM form6 where id= ?";
$sth = $db->prepare($request);
$sth->execute(array($id));
$data = $sth->fetch(PDO::FETCH_ASSOC);
if($data==false){
    header('Location:admin.php');
    exit();
}
session_start();
$_SESSION['login'] = $user_login;
// Записываем ID пользователя.
$_SESSION['uid'] = strip_tags($id);

setcookie('admin','1');
// Делаем перенаправление.
header('Location: index.php');
?>
