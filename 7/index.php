<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */
header('Content-Type: text/html; charset=UTF-8');
include 'dblogin.php';

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  print '<div class=" block"><a href="admin.php">Вход для администратора</a></div>';
  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', time()+365*24*60*60);
    setcookie('login', '', time()+365*24*60*60);
    setcookie('pass', '', time()+365*24*60*60);
    // Выводим сообщение пользователю.
    $messages[] = '<div class="block">Спасибо, результаты сохранены.</div>';
//
//      admin перенаправление
// 
if($_COOKIE['admin']=='1'){
    header('Location:login.php');
}
    // Если в куках есть пароль, то выводим сообщение.

    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('<div class="block">Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.</div>',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
 
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);

  // TODO: аналогично все поля.
  $errors['fio_data'] = !empty($_COOKIE['fio_data_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['email_data'] = !empty($_COOKIE['email_data_error']);
    $errors['bdate'] = !empty($_COOKIE['bd_error']);
    $errors['bdate_data'] = !empty($_COOKIE['bd_data_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);

  // Выдаем сообщения об ошибках.
  if($errors['fio']) {
    setcookie('fio_error','',time()+365*24*60*60);
    $messages[] = '<div> Заполните имя</div>';
}
if($errors['fio_data']){
    setcookie('fio_data_error','',time()+365*24*60*60);
    $messages[] = '<div>некорректное имя</div>';
}
if($errors['email']) {
    setcookie('email_error','',time()+365*24*60*60);
    $messages[]='<div>Введите почту</div>';
}
if($errors['email_data']){
    setcookie('email_data_error','',time()+365*24*60*60);
    $messages[]='<div>Введите корректную почту!</div>';
}
if($errors['bdate']) {
    setcookie('bdate_error','',time()+365*24*60*60);
    $messages[]='<div>Введите дату рождения</div>';
}
if($errors['bdate_data']){
    setcookie('bd_data_error','',time()+365*24*60*60);
    $messages[]='<div>Что-то не сходится c датой рождения, попробуйте заново</div>';
}
if($errors['sex']) {
    setcookie('sex_error','',time()+365*24*60*60);
    $messages[]='<div>Выберите свой пол</div>';
}
if($errors['limb'])
{
    setcookie('limb_error','',time()+365*24*60*60);
    $messages[] = '<div>Выберите количество конечностей</div>';
}
if($errors['bio']) {
    setcookie('bio_error','',time()+365*24*60*60);
    $messages[]='<div>Расскажите о себе</div>';
}
if($errors['check']) {
    setcookie('check_error','',time()+365*24*60*60);
    $messages[] = '<div>Подтвердите согласие на обработку персональных данных</div>';
}


  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' :  strip_tags($_COOKIE['email_value']);
  $values['bdate'] = empty($_COOKIE['bdate_value']) ? '' : strip_tags($_COOKIE['bdate_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limb'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
  $values['check'] = empty($_COOKIE['check_value']) ? '' : intval($_COOKIE['check_value']);
  $values['godmode'] = empty($_COOKIE['godmode_value']) ? '':1;
  $values['wallhack'] = empty($_COOKIE['wallhack_value']) ? '':1;
  $values['noclip'] =  empty($_COOKIE['noclip_value']) ? '':1;

  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) 
      {
          // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        // предварительно санитизовав.
    $login = $db->quote($_SESSION['login']);
    $stmt = $db->prepare("SELECT id FROM form6 WHERE login = ?"); //$login
    $stmt->execute(array($login));
    $user_id='';
    while($row = $stmt->fetch())
    {
        $user_id=$row['id'];
    }

    $request = "SELECT id,fio,email,bdate,sex,limb,bio,checkbox FROM form WHERE id = ?";
    $result = $db -> prepare($request);
    $result ->execute(array($user_id));
    $data = $result->fetch(PDO::FETCH_ASSOC);

    $values['fio'] = strip_tags($data['fio']);
    $values['email'] = strip_tags($data['email']);
    $values['bdate'] = strip_tags($data['bdate']);
    $values['sex'] = strip_tags($data['sex']);
    $values['limb'] = strip_tags($data['limb']);
    $values['bio'] = strip_tags($data['bio']);
    $values['check'] = intval($data['checkbox']);
    $id = $db->quote($data['id']);
    $query = "SELECT superpower FROM superpower6 WHERE id= ?";
    $result = $db->prepare($query);
    $result->execute(array($id));
    $data = $result->fetchAll();
    foreach($data as $inf){
        $values[$inf['superpower']]=1;
    }

    printf('Вход с логином %s, login %d', $_SESSION['login'], $_SESSION['uid']);
    print '<div class="block"><br/><a href="login.php">Нажмите</a> для выхода';
  }else{
     print '<div class="block"><br/><a href="login.php">Нажмите</a> чтобы войти под учетной записью</div>';
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    $superp_info = array('godmode','wallhack','noclip');
    if (!isset($_COOKIE['admin'])) {
        setcookie('admin', '0');
    }
  $errors = FALSE;

  /// проверка регулярными выражениями и сохранение печенек
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
} elseif (!preg_match('/[а-яёА-ЯЁ]{1,}+$/', $_POST['fio'])) {
    setcookie('fio_data_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
} else {
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
}

if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
} elseif (!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['email'])) {
    setcookie('email_data_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
} else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
}
    if(empty($_POST['bdate'])) {
        setcookie('bdate_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('bdate_value',$_POST['bdate'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['sex'])) {
        setcookie('sex_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('sex_value',$_POST['sex'],time()+365*24*60*60); // сохр на год
    }

    if(empty($_POST['limb'])) {
        setcookie('limb_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('limb_value',$_POST['limb'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['bio'])) { 
        setcookie('bio_error','1',time()+24*60*60);
        $errors = TRUE;
    }   else {
        setcookie('bio_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
    }





    if(empty($_POST['checkbox'])) {
        setcookie('check_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('check_value',$_POST['checkbox'],time()+365*24*60*60); // сохр на год
    }
    $superpower =$_POST['superpower'];
    foreach($superpower as $supra) {
        if (in_array($supra, $superp_info)) {
            setcookie($supra . '_value', '1', time() + 365 * 24 * 60 * 60);
        }
    }
    foreach($superp_info as $s_i){
        if(!in_array($s_i,$superpower)){
           setcookie($s_i.'_value','',time()+365*24*60*60);
        }
    }
  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    
    setcookie('fio_error', '', time()+365*24*60*60);
  
    setcookie('email_error', '', time()+365*24*60*60);
    setcookie('bdate_error','',time()+365*24*60*60);
    setcookie('sex_error','',time()+365*24*60*60);
    setcookie('limb_error','',time()+365*24*60*60);
    setcookie('bio_error', '', time()+365*24*60*60);
    setcookie('check_error','',time()+365*24*60*60);
        foreach($superp_info as $s_i){
            setcookie($s_i.'_value','',time()+365*24*60*60);
        }
  }
//3
  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if(!empty($_COOKIE[session_name()]) 
  && session_start() 
  && !empty($_SESSION['login'])) {
   
   $fio = $db->quote($_POST['fio']);
   $email = $db->quote($_POST['email']);
   $bdate = $db->quote($_POST['bdate']);
   $sex = $db->quote($_POST['sex']);
   $limb = $db->quote($_POST['limb']);
   $bio = $db->quote($_POST['bio']) ;
   $login = $db->quote($_SESSION['login']);
   $requestUpdate = "UPDATE form6 SET fio=?,email=?,bdate=?,sex=?,limb=?,bio=? WHERE login=?";
   $requestInsert = $db->prepare($requestUpdate);
   $requestInsert->execute(array(
    $_POST['fio'],
    $_POST['email'],
    $_POST['bdate'],
    $_POST['sex'],
    $_POST['limb'],
    $_POST['bio'],
    $_SESSION['login']
));

   $request = "SELECT id from form6 WHERE login= ?";
   $reqSel = $db->prepare($request);
   $reqSel->execute(array($login));
   $data_id = $reqSel->fetch(PDO::FETCH_ASSOC);
   // удаление старой инфы и вставка новой в superpower6
    $id = intval($data_id['id']);
    $requestInsert = $db->prepare("DELETE from superpower6 where id= ? ");
    $requestInsert->execute(array($id));
    $superpower = $_POST['superpower'];
    $superp_info = array('godmode', 'wallhack', 'noclip');
    foreach($superp as $sup){
     if(in_array($sup,$superp_info)){
         $requestInsert = $db->prepare("INSERT INTO superpower6(id,superpower) VALUES($id,:superp)");
         $requestInsert->execute(array('superp'=>$sup));
     }
}
}
else{
    $login = uniqid();
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $pass = '';
    // пароль состоит из 12 символов типа string
    for ($i = 0; $i < 12; $i++) {
        $pass .= $characters[rand(0, $charactersLength - 1)];
    }
    //вывод логина и пароля на экран для пользователя
    $messages[] ='<div class="block">
    Ваш логин <br>' .
    $login .
    '<br> 
    Ваш пароль 
    <br>' .
    $pass .
    '</div>';
    setcookie('login',$login);
    setcookie('pass',$pass);
    $passwordHash = password_hash($pass,PASSWORD_DEFAULT);

    // отправка данных на сервер
    $requestInsert = $db->prepare("INSERT INTO form6 (fio,email,bdate,sex,limb,bio,checkbox,login,pass) VALUES (:fio, :email,:bdate,:sex,:limb,:bio,:checkbox,:login,:pass)");
    $requestInsert -> execute(array('fio'=>$_POST['fio'], 'email'=>$_POST['email'],'bdate'=>$_POST['bdate'],'sex'=>$_POST['sex'],'limb'=>$_POST['limb'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox'],'login' =>$login, 'pass' => $passwordHash));

    $requestInsert = $db->prepare("SELECT id from form6 WHERE login=?");
    $requestInsert->execute(array($login));
    // извлечение следующего id для соединения таблиц с помощью fetch()
    $id1 = $requestInsert->fetch();
    $id = $id1[0];
    $superp = $_POST['superpower'];
    
    foreach($superp as $sup){
        if(in_array($sup,$superp_info)){
            $requestInsert = $db->prepare("INSERT INTO superpower6(id,superpower) VALUES(:id,:superp)");
            $requestInsert->execute(array('id'=>$id,'superp'=>$sup));
        }
    }


}
setcookie('save', '1');

header('Location: index.php');

}

