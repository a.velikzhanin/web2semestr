<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  session_destroy();
  // Делаем перенаправление на форму.
  header('Location: index.php');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if(!empty($_SESSION['error_session'])){
      $msg = $_SESSION['error_session'];
      print("<div>'$msg'</div>");
      $_SESSION['error_session']="";
  }
  ?>
  <form action="login.php" method="post">
      <input name="login" />
      <input name="password" />
      <input type="submit" value="Войти" />
  </form>

  <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u20936';
  $pass = '1532365';
  $db = new PDO('mysql:host=localhost;dbname=u20936', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt = $db->prepare("SELECT id FROM form1 WHERE login=:i");
  $stmt->execute();
  $zap = $db ->query("SELECT COUNT(*) FROM form1");
  $count = $zap ->fetchColumn();
  $f=0;
    $data = $stmt->fetchAll();
    for($i=0;$i<$count;$i++)
    {
        if($data[$i]['login']==$_POST['login']){
            $f=1;
            if($data[$i]['pass']==md5($_POST['pass'])){
                $_SESSION['error_session']="";
                $id= $data[$i]['id'];
                break;
            }else{
                $_SESSION['error_session'] = "issues with password";
                header('Location:login.php');
                exit();
            }
        }
    }
    if($f==0){
        $_SESSION['error_session'] = "issues with login";
        header('Location:login.php');
        exit();
    }
    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uid'] = $id;
    // Делаем перенаправление.
    header('Location: index.php');
}
