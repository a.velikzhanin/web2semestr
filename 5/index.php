<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

header('Content-Type: text/html; charset=UTF-8');
$user = 'u20936';
$pass = '1532365';
$db = new PDO('mysql:host=localhost;dbname=u20936', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = '<div class="block">Спасибо, результаты сохранены.</div>';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('<div class="block">Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.</div>',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
 
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);

  // TODO: аналогично все поля.
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['bdate'] = !empty($_COOKIE['bdate_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  // Выдаем сообщения об ошибках.
  if($errors['fio']) {
    setcookie('fio_error','',100000);
    $messages[] = '<div> Заполните имя</div>';
}
if($errors['email']) {
    setcookie('email_error','',100000);
    $messages[]='<div>Введите почту</div>';
}
if($errors['bdate']) {
    setcookie('bdate_error','',100000);
    $messages[]='<div>Введите дату рождения</div>';
}
if($errors['sex']) {
    setcookie('sex_error','',100000);
    $messages[]='<div>Выберите свой пол</div>';
}
if($errors['limbs'])
{
    setcookie('limbs_error','',100000);
    $messages[] = '<div>Выберите количество конечностей</div>';
}
if($errors['bio']) {
    setcookie('bio_error','',100000);
    $messages[]='<div>Расскажите о себе</div>';
}
if($errors['check']) {
    setcookie('check_error','',100000);
    $messages[] = '<div>Подтвердите согласие на обработку персональных данных</div>';
}






  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.
  $values['email'] = empty($_COOKIE['email_value']) ? '' :  strip_tags($_COOKIE['email_value']);
  $values['bdate'] = empty($_COOKIE['bdate_value']) ? '' : $_COOKIE['bdate_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];

  
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    $user = 'u20936';
    $pass = '1532365';
    $db = new PDO('mysql:host=localhost;dbname=u20936', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // и заполнить переменную $values,
    // предварительно санитизовав.
    $log = $_SESSION['login'];
    $request = "SELECT fio,email,bdate,sex,limb,bio,checkbox FROM form1 WHERE login = '$log'";
    $result = $db -> prepare($request);
    $result ->execute();
    $data = $result->fetch(PDO::FETCH_ASSOC);
    $values['fio'] = strip_tags($data['fio']);
    $values['email'] = strip_tags($data['email']);
    $values['bdate'] = strip_tags($data['bdate']);
    $values['sex'] = strip_tags($data['sex']);
    $values['limbs'] = $data['limb'];
    $values['bio'] = strip_tags($data['bio']);
    $values['check'] = strip_tags($data['checkbox']);

    printf('Вход с логином %s, login %d', $_SESSION['login'], $_SESSION['user']);
    print '<div class="block"><br/><a href="login.php">Нажмите</a> для выхода';
  }else{
     print '<div class="block"><br/><a href="login.php">Нажмите</a> чтобы войти с под учетной записью</div>';
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  $errors = FALSE;
    if(empty($_POST['fio'])) {
        setcookie('fio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value',$_POST['fio'],time()+365*24*60*60); // сохр на год
    }

    if(empty($_POST['email'])) {
        setcookie('email_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value',$_POST['email'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['bdate'])) {
        setcookie('bdate_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('bdate_value',$_POST['bdate'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['radio2'])) {
        setcookie('sex_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('sex_value',$_POST['radio2'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['limb'])) {
        setcookie('limbs_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('limbs_value',$_POST['limb'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['bio'])) { 
        setcookie('bio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    if(empty($_POST['checkbox'])) {
        setcookie('check_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('check_value',$_POST['checkbox'],time()+365*24*60*60); // сохр на год
    }

  







  if ($errors) {
    
    header('Location: index.php');
    exit();
  }
  else {
    
    setcookie('fio_error', '', 100000);
  
    setcookie('email_error', '', 100000);
        setcookie('bdate_error','',100000);
        setcookie('sex_error','',100000);
        setcookie('limbs_error','',100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error','',100000);
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if(!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
   
   $fio = $_POST['fio'];
   $email = $_POST['email'];
   $bdate = $_POST['bdate'];
   $sex = $_POST['sex'];
   $limb = $_POST['limb'];
   $bio = $_POST['bio'] ;
   $lg = $_SESSION['login'];

   $sql = "UPDATE form1 SET fio='$fio',email='$email',year='$bdate',sex='$sex',limb='$limb',bio='$bio' WHERE login='$lg'";
   $stmt = $db->prepare($sql);
   $stmt->execute();

   $r = "SELECT id from form1 WHERE login='$lg'";
   $s = $db->prepare($r);
   $s->execute();
   $data_id = $s->fetch(PDO::FETCH_ASSOC);
   $id = $data_id['id'];
   $abil = $_POST['select1'];
    $imm=0;
    $ph=0;
    $lv=0;
   if(count($abil)==3){
       $imm=1;
       $ph=1;
       $lv=1;
   }
   elseif(count($abil)==2){
       if($abil[0]==1 && $abil[1]==2){
           $imm=1;
           $ph=1;
       }
       if($abil[0]==1 && $abil[1]==3){
           $imm=1;
           $lv=1;
       }
       if($abil[0]==2 && $abil[1]==3){
           $ph=1;
           $lv=1;
       }
   }
   elseif(count($abil)==1){
       if($abil[0]==1)$imm=1;
       if($abil[0]==2)$ph=1;
       if($abil[0]==3)$lv=1;
   }
   $sql = "UPDATE superpower1 SET godmode='$imm',wallhack='$ph',noclip='$lv' WHERE id='$id'";
   $stmt = $db->prepare($sql);
   $stmt->execute();
}
else{
    $login = uniqid();
    $pass = strval(rand());
    $messages[] ='<div class="block">
    Ваш логин <br>' .
    $login .
    '<br> 
    Ваш пароль 
    <br>' .
    $pass .
    '</div>';
    setcookie('login',$login);
    setcookie('pass',$pass);
    $pass_for_bd = md5($pass);
    $stmt = $db->prepare("INSERT INTO form1 (fio, email,year,sex,limb,bio,checkbox,login,pass) VALUES (:fio, :email,:bdate,:sex,:limb,:bio,:checkbox,:login,:pass)");
    $stmt -> execute(array('fio'=>$_POST['fio'], 'email'=>$_POST['email'],'bdate'=>$_POST['bdate'],'sex'=>$_POST['sex'],'limb'=>$_POST['limb'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox'],'login' =>$login, 'pass' => $pass_for_bd));


    $stmt=$db->prepare("INSERT INTO superpower1(godmode,wallhack,noclip) VALUES(:imm,:ph,:lv)");
    $abil = $_POST['select1'];
    $imm=0;
    $ph=0;
    $lv=0;
    if(count($abil)==3){
        $imm=1;
        $ph=1;
        $lv=1;
    }
    elseif(count($abil)==2){
        if($abil[0]==1 && $abil[1]==2){
            $imm=1;
            $ph=1;
        }
        if($abil[0]==1 && $abil[1]==3){
            $imm=1;
            $lv=1;
        }
        if($abil[0]==2 && $abil[1]==3){
            $ph=1;
            $lv=1;
        }
    }
    elseif(count($abil)==1){
        if($abil[0]==1)$imm=1;
        if($abil[0]==2)$ph=1;
        if($abil[0]==3)$lv=1;
    }
    $stmt->execute(array('imm'=>$imm,'ph'=>$ph,'lv'=>$lv));
}
setcookie('save', '1');

header('Location: index.php');

}

  /*

$length = 6;
    			
      $chars = 'qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP'; 
      $size = strlen($chars) - 1; 
      $pass = ''; 
      $login='';
      while($length--) {
        $pass .= $chars[random_int(0, $size)]; 
        $login .= $chars[random_int(0, $size)];
      }
      // вывод логина и пароля
      print '<div>
      Ваш логин <br>' .
      $login .
      '<br> 
      Ваш пароль 
      <br>' .
      $pass .
      '</div>';
    
  */

