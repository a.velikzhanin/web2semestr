<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
// Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['bdate'] = !empty($_COOKIE['bdate_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);


    if($errors['fio']) {
        setcookie('fio_error','',100000);
        $messages[] = '<div> Заполните имя</div>';
    }
    if($errors['email']) {
        setcookie('email_error','',100000);
        $messages[]='<div>Введите почту</div>';
    }
    if($errors['bdate']) {
        setcookie('bdate_error','',100000);
        $messages[]='<div>Введите дату рождения</div>';
    }
    if($errors['sex']) {
        setcookie('sex_error','',100000);
        $messages[]='<div>Выберите свой пол</div>';
    }
    if($errors['limbs'])
    {
        setcookie('limbs_error','',100000);
        $messages[] = '<div>Выберите количество конечностей</div>';
    }
    if($errors['bio']) {
        setcookie('bio_error','',100000);
        $messages[]='<div>Расскажите о себе</div>';
    }
    if($errors['check']) {
        setcookie('check_error','',100000);
        $messages[] = '<div>Подтвердите согласие на обработку персональных данных</div>';
    }

    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['bdate'] = empty($_COOKIE['bdate_value']) ? '' : $_COOKIE['bdate_value'];
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];

    include('form.php');
}
// если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    $errors = FALSE;
    if(empty($_POST['fio'])) {
        setcookie('fio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value',$_POST['fio'],time()+365*24*60*60); // сохр на год
    }

    if(empty($_POST['email'])) {
        setcookie('email_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value',$_POST['email'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['bdate'])) {
        setcookie('bdate_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('bdate_value',$_POST['bdate'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['radio2'])) {
        setcookie('sex_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('sex_value',$_POST['radio2'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['limb'])) {
        setcookie('limbs_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('limbs_value',$_POST['limb'],time()+365*24*60*60); // сохр на год
    }
    if(empty($_POST['bio'])) { 
        setcookie('bio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    if(empty($_POST['checkbox'])) {
        setcookie('check_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('check_value',$_POST['checkbox'],time()+365*24*60*60); // сохр на год
    }

    if($errors) {
        header('Location:index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('bdate_error','',100000);
        setcookie('sex_error','',100000);
        setcookie('limbs_error','',100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error','',100000);
    }

    setcookie('save', '1');

    $user = 'u20936';
    $pass = '1532365';
    $db = new PDO('mysql:host=localhost;dbname=u20936', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    
    
    // Подготовленный запрос. Не именованные метки.
    
    $stmt =$db->prepare("INSERT INTO form (fio,email,bdate,sex,limb,bio,checkbox) VALUES (:fio,:email,:bdate,:sex,:limb,:bio,:checkbox)");
    $stmt ->execute(array('fio'=>$_POST['fio'],'email'=>$_POST['email'], 'bdate'=>$_POST['bdate'],'sex'=>$_POST['sex'],'limb'=>$_POST['limb'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox']));
    
    
    
    //  stmt - это "дескриптор состояния".
    
    //  Именованные метки.
    $stmt=$db->prepare("INSERT INTO superpower(godmode,wallhack,noclip) VALUES(:godmode,:wallhack,:noclip");
    
    $sp=$_POST['superpower'];
    
    
    for($i=0;$i<3;$i++)
    {
    if($sp[$i]!=1)
    {
      $sp[$i]=0;
    }
    print $sp[$i];
    }
    
    //запись в бд
    $stmt->execute(array('godmode'=>$sp[0],'wallhack'=>$sp[1],'noclip'=>$sp[2]));
    
    header('Location:index.php');
}

//
// отправка
//


